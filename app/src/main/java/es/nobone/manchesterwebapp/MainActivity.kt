package es.nobone.manchesterwebapp

import android.Manifest
import android.content.pm.PackageManager
import android.net.http.SslError
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.webkit.*
import android.webkit.WebSettings.LOAD_NO_CACHE
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var webView: WebView? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var pressed: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        createSSLInstance()
        checkPermission()
        initWebViewSettings()
        initWebView()

    }

    private fun initWebView() {
        webView?.loadUrl("https://odour.100x100.net/")
    }

    private fun initWebViewSettings() {
        val settings = webView?.getSettings()

        settings?.javaScriptEnabled = true
        settings?.javaScriptCanOpenWindowsAutomatically = true
        settings?.pluginState = android.webkit.WebSettings.PluginState.ON
        settings?.domStorageEnabled = true
        settings?.setCacheMode(LOAD_NO_CACHE)

    }

    private fun createSSLInstance() {
        webView!!.webViewClient = SSLTolerentWebViewClient()

    }

    private fun initViews() {
        webView = findViewById(R.id.webview) as WebView
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        spin_kit.visibility = View.VISIBLE

    }

    private fun checkPermission(){

        if (((ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                        && (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED)))
        {

            mFusedLocationClient?.getLastLocation()
                    ?.addOnSuccessListener(this) { location -> // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            println(location.latitude)
                            println(location.longitude)

                        }
                    }
        }
        else
        {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 0)

        }

        webView?.webChromeClient = object : WebChromeClient() {
            override fun onGeolocationPermissionsShowPrompt(origin: String, callback: GeolocationPermissions.Callback) {
                callback.invoke(origin, true, false)

                spin_kit.visibility = View.GONE

            }
        }
    }

    private inner class SSLTolerentWebViewClient : WebViewClient() {

        override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
            val builder = AlertDialog.Builder(applicationContext)
            var message = "SSL Certificate error."
            when (error.primaryError) {
                SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                SslError.SSL_EXPIRED -> message = "The certificate has expired."
                SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
            }
            message += " Do you want to continue anyway?"

            builder.setTitle("SSL Certificate Error")
            builder.setMessage(message)
            builder.setPositiveButton("continue") { dialog, which -> handler.proceed() }
            builder.setNegativeButton("cancel") { dialog, which -> handler.cancel() }
            val dialog = builder.create()
            dialog.show()
        }
    }

     override fun onBackPressed() {

         if (pressed == 0) {

             val text = "Pulse de nuevo para salir"

             val toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT)
             toast.setGravity(Gravity.CENTER or Gravity.CENTER, 0, 0)
             toast.show()
             pressed = 1

         } else {

             finish()
             moveTaskToBack(true)
             pressed = 0
         }
    }
}
